import Vue from 'vue'
import Router from 'vue-router'
import HelloWorld from '@/components/HelloWorld'
import SecondPage from '@/components/SecondPage'
Vue.use(Router)
/*
  Exempel för att sätta upp olika urler.
   mode: 'history' för att få url:er som
  http://localhost:8080/second-page
   istället för
   http://localhost:8080/#/second-page
*/
export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'HelloWorld',
      component: HelloWorld
    },
    {
      path: '/second-page',
      name: 'SecondPage',
      component: SecondPage
    }
  ]
})
